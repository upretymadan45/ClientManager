package com.helper.little.uprety45.userinfo.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;

import com.helper.little.uprety45.userinfo.R;
import com.helper.little.uprety45.userinfo.activities.ClientDetailsActivity;


public class YesNoConfirmationDialog extends DialogFragment {
    private IOnDialogButtonClickListener iOnDialogButtonClickListener;
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Confirmation");



        builder.setView(getActivity().getLayoutInflater().inflate(R.layout.yes_no_confirmation,null));

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                iOnDialogButtonClickListener.onPositiveButtonClick();
            }
        })
         .setNegativeButton("No", new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialog, int which) {
                 getDialog().dismiss();
             }
         });

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(isAdded()){
            FragmentManager fragmentManager = getFragmentManager();
            FamilyMemberDetailsDialog familyMemberDetailsDialog = (FamilyMemberDetailsDialog) fragmentManager.findFragmentByTag(FamilyMemberDetailsDialog.TAG);
            if(familyMemberDetailsDialog!=null)
                iOnDialogButtonClickListener = familyMemberDetailsDialog;
            else
                iOnDialogButtonClickListener = (ClientDetailsActivity)getActivity();
        }
    }

    public interface IOnDialogButtonClickListener{
        void onPositiveButtonClick();
    }
}
