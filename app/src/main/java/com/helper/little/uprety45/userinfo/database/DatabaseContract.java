package com.helper.little.uprety45.userinfo.database;

import android.provider.BaseColumns;

/**
 * Created by madanuprety on 22/11/17.
 */

public final class DatabaseContract {

    private DatabaseContract() {}

    public static final class FamilyHeadEntry implements BaseColumns{
        public static final String TABLE_NAME = "family_head";
        public static final String COLUMN_HEAD_FIRST_NAME = "first_name";
        public static final String COLUMN_HEAD_MIDDLE_NAME = "middle_name";
        public static final String COLUMN_HEAD_LAST_NAME = "last_name";
        public static final String COLUMN_DATE_OF_BIRTH = "date_of_birth";
        public static final String COLUMN_CITIZENSHIP_NUMBER = "citizenship_number";
        public static final String COLUMN_HEAD_IMAGE_URL = "image_url";

        public static final String SQL_CREATE_TABLE = "CREATE TABLE "+ TABLE_NAME + "(" +
                                                        _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                                                        COLUMN_HEAD_FIRST_NAME +" TEXT NOT NULL, "+
                                                        COLUMN_HEAD_MIDDLE_NAME+" TEXT, "+
                                                        COLUMN_HEAD_LAST_NAME+" TEXT NOT NULL, "+
                                                        COLUMN_DATE_OF_BIRTH+" TEXT NOT NULL, "+
                                                        COLUMN_CITIZENSHIP_NUMBER+" TEXT, "+
                                                        COLUMN_HEAD_IMAGE_URL +" TEXT"+")";
    }

    public static final class FamilyMemberEntry implements BaseColumns{
        public static final String TABLE_NAME = "family_member";
        public static final String COLUMN_MEMBER_RELATION_TO_HEAD = "relation_to_head";
        public static final String COLUMN_MEMBER_FIRST_NAME = "first_name";
        public static final String COLUMN_MEMBER_MIDDLE_NAME = "middle_name";
        public static final String COLUMN_MEMBER_LAST_NAME = "last_name";
        public static final String COLUMN_DATE_OF_BIRTH = "date_of_birth";
        public static final String COLUMN_CITIZENSHIP_NUMBER = "citizenship_number";
        public static final String COLUMN_MEMBER_IMAGE_URL = "image_url";
        public static final String COLUMN_HEAD_ID = "head_id";

        public static final String SQL_CREATE_TABLE = "CREATE TABLE "+ TABLE_NAME + "(" +
                _ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "+
                COLUMN_MEMBER_RELATION_TO_HEAD +" TEXT NOT NULL, "+
                COLUMN_MEMBER_FIRST_NAME+" TEXT NOT NULL, "+
                COLUMN_MEMBER_MIDDLE_NAME+" TEXT, "+
                COLUMN_MEMBER_LAST_NAME+" TEXT NOT NULL, "+
                COLUMN_DATE_OF_BIRTH+" TEXT NOT NULL, "+
                COLUMN_CITIZENSHIP_NUMBER+" TEXT, "+
                COLUMN_MEMBER_IMAGE_URL+" TEXT, "+
                COLUMN_HEAD_ID+" INTEGER , "+
                " FOREIGN KEY ("+ COLUMN_HEAD_ID +") REFERENCES "+
                FamilyHeadEntry.TABLE_NAME +"("+ FamilyHeadEntry._ID +")" +")";
    }
}
