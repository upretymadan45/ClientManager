package com.helper.little.uprety45.userinfo.fragments;


import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.helper.little.uprety45.userinfo.R;
import com.helper.little.uprety45.userinfo.activities.ClientDetailsActivity;
import com.helper.little.uprety45.userinfo.database.UserInfoOpenHelper;
import com.helper.little.uprety45.userinfo.helpers.ImageFileCreator;
import com.helper.little.uprety45.userinfo.models.ClientModel;

import java.io.File;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;
import static com.helper.little.uprety45.userinfo.database.DatabaseContract.FamilyHeadEntry;

public class AddFamilyHeadFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = AddFamilyHeadFragment.class.getSimpleName();
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private Button btnSave, btnTakePicture;
    private EditText firstName, middleName,lastName,citizenshipNumber,dateOfBirth;
    private UserInfoOpenHelper userInfoOpenHelper;
    private String capturedImageUrl;
    private boolean isEditMode = false;
    private int headId;

    public AddFamilyHeadFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_family_head, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userInfoOpenHelper = new UserInfoOpenHelper(getActivity());

        btnSave = (Button) view.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

        btnTakePicture = (Button) view.findViewById(R.id.btnTakePicture);
        btnTakePicture.setOnClickListener(this);

        firstName = (EditText) view.findViewById(R.id.first_name);
        middleName = (EditText) view.findViewById(R.id.middle_name);
        lastName = (EditText) view.findViewById(R.id.last_name);
        citizenshipNumber = (EditText) view.findViewById(R.id.citizenship_number);
        dateOfBirth = (EditText) view.findViewById(R.id.date_of_birth);

        Bundle bundle = getArguments();
        if(bundle==null) return;
        ClientModel clientModel = bundle.getParcelable("ClientModel");
        if(clientModel!=null){
            firstName.setText(clientModel.getFirstName());
            middleName.setText(clientModel.getMiddleName());
            lastName.setText(clientModel.getLastName());
            citizenshipNumber.setText(clientModel.getCitizenshipNumber());
            dateOfBirth.setText(clientModel.getDateOfBirth());
            headId = clientModel.getId();
            isEditMode = true;
        }
    }

    @Override
    public void onClick(View v) {
        if(v==btnSave) {
            if (firstName.getText().toString().equals("")) {
                firstName.setError("Cannot be blank");
                return;
            }

            if (lastName.getText().toString().equals("")) {
                lastName.setError("Cannot be blank");
                return;
            }

            if (citizenshipNumber.getText().toString().equals("")) {
                citizenshipNumber.setError("Cannot be blank");
                return;
            }

            if (dateOfBirth.getText().toString().equals("")) {
                dateOfBirth.setError("Cannot be blank");
                return;
            }

            int createdHeadId = saveFamilyHead();

            if(!isEditMode){
                if (createdHeadId > 0) {
                    Toast.makeText(getActivity(), "Successfully Saved", Toast.LENGTH_LONG).show();
                    AddFamilyMemberFragment fragment = new AddFamilyMemberFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("headId", createdHeadId);
                    fragment.setArguments(bundle);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.group, fragment, AddFamilyMemberFragment.TAG);
                    fragmentTransaction.commit();
                }
            } else {
                isEditMode = false;
                Intent intent = new Intent(getActivity(), ClientDetailsActivity.class);
                intent.putExtra("headId",headId);
                startActivity(intent);
            }
        }

        if(v==btnTakePicture){
                Log.d(TAG, "onClick: take picture");
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = ImageFileCreator.createImageFile(getActivity());
                        capturedImageUrl = photoFile.getAbsolutePath();
                    } catch (IOException ex) {

                    }
                    if (photoFile != null) {
                        Uri photoURI = FileProvider.getUriForFile(getActivity(),
                                "com.helper.little.uprety45.fileprovider",
                                photoFile);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
            }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode==RESULT_OK){
            Toast.makeText(getActivity(),"Photo successfully caputred",Toast.LENGTH_LONG).show();
        }
    }

    public static AddFamilyHeadFragment getFragmentInstance(){
        return new AddFamilyHeadFragment();
    }

    @Override
    public void onDetach() {
        if(userInfoOpenHelper!=null)
            userInfoOpenHelper.close();
        super.onDetach();
    }

    public int saveFamilyHead(){
        ContentValues values = new ContentValues();
        values.put(FamilyHeadEntry.COLUMN_HEAD_FIRST_NAME,firstName.getText().toString());
        values.put(FamilyHeadEntry.COLUMN_HEAD_MIDDLE_NAME,middleName.getText().toString());
        values.put(FamilyHeadEntry.COLUMN_HEAD_LAST_NAME,lastName.getText().toString());
        values.put(FamilyHeadEntry.COLUMN_CITIZENSHIP_NUMBER,citizenshipNumber.getText().toString());
        values.put(FamilyHeadEntry.COLUMN_DATE_OF_BIRTH,dateOfBirth.getText().toString());
        if(capturedImageUrl!=null)
        values.put(FamilyHeadEntry.COLUMN_HEAD_IMAGE_URL,capturedImageUrl.toString());

        if(!isEditMode){
            SQLiteDatabase db = userInfoOpenHelper.getWritableDatabase();
            return (int)db.insert(FamilyHeadEntry.TABLE_NAME,null,values);
        } else {
            String selection = FamilyHeadEntry._ID + " =? ";
            String selectionArgs[] = {String.valueOf(headId)};
            SQLiteDatabase db = userInfoOpenHelper.getWritableDatabase();
            return (int)db.update(FamilyHeadEntry.TABLE_NAME,values,selection,selectionArgs);
        }

    }
}
