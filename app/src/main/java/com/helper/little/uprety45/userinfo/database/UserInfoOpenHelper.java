package com.helper.little.uprety45.userinfo.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by madanuprety on 23/11/17.
 */

public class UserInfoOpenHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "userInfo.db";
    public static final int DATABASE_VERSION = 1;

    public UserInfoOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DatabaseContract.FamilyHeadEntry.SQL_CREATE_TABLE);
        db.execSQL(DatabaseContract.FamilyMemberEntry.SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
