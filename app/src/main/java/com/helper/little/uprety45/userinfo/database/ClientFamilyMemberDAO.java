package com.helper.little.uprety45.userinfo.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.helper.little.uprety45.userinfo.models.ClientFamilyMemberModel;

import java.util.ArrayList;
import java.util.List;

public final class ClientFamilyMemberDAO {

    private Context context;
    private static List<ClientFamilyMemberModel> clientFamilyMemberModelList;
    private static UserInfoOpenHelper userInfoOpenHelper;

    public ClientFamilyMemberDAO(Context context) {
        this.context = context;
        clientFamilyMemberModelList = new ArrayList<ClientFamilyMemberModel>();
        userInfoOpenHelper = new UserInfoOpenHelper(context);
    }

    public static final List<ClientFamilyMemberModel> loadClientFamilyMembers(String headId){
        SQLiteDatabase db = userInfoOpenHelper.getReadableDatabase();

        String [] projection = {DatabaseContract.FamilyMemberEntry.COLUMN_MEMBER_FIRST_NAME,
                DatabaseContract.FamilyMemberEntry.COLUMN_MEMBER_RELATION_TO_HEAD,
                DatabaseContract.FamilyHeadEntry.COLUMN_HEAD_IMAGE_URL,
                DatabaseContract.FamilyMemberEntry._ID};

        String selection = DatabaseContract.FamilyMemberEntry.COLUMN_HEAD_ID + " = ?";

        String selectionArgs[] = {headId};

        Cursor cursor = db.query(DatabaseContract.FamilyMemberEntry.TABLE_NAME,projection,selection,selectionArgs,null,null, DatabaseContract.FamilyMemberEntry.COLUMN_MEMBER_FIRST_NAME);

        int clientIdPos = cursor.getColumnIndex(DatabaseContract.FamilyMemberEntry._ID);
        int clientFirstNamePos = cursor.getColumnIndex(DatabaseContract.FamilyMemberEntry.COLUMN_MEMBER_FIRST_NAME);
        int clientRelationToHead = cursor.getColumnIndex(DatabaseContract.FamilyMemberEntry.COLUMN_MEMBER_RELATION_TO_HEAD);
        int clientImageUrlPos = cursor.getColumnIndex(DatabaseContract.FamilyMemberEntry.COLUMN_MEMBER_IMAGE_URL);

        while(cursor.moveToNext()){

            ClientFamilyMemberModel client = new ClientFamilyMemberModel();
            client.setFirstName(cursor.getString(clientFirstNamePos));
            client.setId(cursor.getInt(clientIdPos));
            client.setRelationToHead(cursor.getString(clientRelationToHead));
            client.setImageUrl(cursor.getString(clientImageUrlPos));

            clientFamilyMemberModelList.add(client);
        }

        userInfoOpenHelper.close();

        return clientFamilyMemberModelList;
    }

    public static final ClientFamilyMemberModel getClientFamilyMember(String id){
        SQLiteDatabase db = userInfoOpenHelper.getReadableDatabase();

        String [] projection = {DatabaseContract.FamilyMemberEntry.COLUMN_MEMBER_FIRST_NAME,
                DatabaseContract.FamilyMemberEntry.COLUMN_MEMBER_MIDDLE_NAME,
                DatabaseContract.FamilyMemberEntry.COLUMN_MEMBER_LAST_NAME,
                DatabaseContract.FamilyMemberEntry.COLUMN_DATE_OF_BIRTH,
                DatabaseContract.FamilyMemberEntry.COLUMN_CITIZENSHIP_NUMBER,
                DatabaseContract.FamilyMemberEntry.COLUMN_MEMBER_RELATION_TO_HEAD,
                DatabaseContract.FamilyHeadEntry.COLUMN_HEAD_IMAGE_URL,
                DatabaseContract.FamilyMemberEntry._ID,
                DatabaseContract.FamilyMemberEntry.COLUMN_HEAD_ID};

        String selection = DatabaseContract.FamilyMemberEntry._ID + " = ?";

        String selectionArgs[] = {String.valueOf(id)};

        Cursor cursor = db.query(DatabaseContract.FamilyMemberEntry.TABLE_NAME,projection,selection,selectionArgs,null,null, null);

        int clientIdPos = cursor.getColumnIndex(DatabaseContract.FamilyMemberEntry._ID);
        int clientFirstNamePos = cursor.getColumnIndex(DatabaseContract.FamilyMemberEntry.COLUMN_MEMBER_FIRST_NAME);
        int clientMiddleNamePos = cursor.getColumnIndex(DatabaseContract.FamilyMemberEntry.COLUMN_MEMBER_MIDDLE_NAME);
        int clientLastNamePos = cursor.getColumnIndex(DatabaseContract.FamilyMemberEntry.COLUMN_MEMBER_LAST_NAME);
        int clientDateOfBirthPos = cursor.getColumnIndex(DatabaseContract.FamilyMemberEntry.COLUMN_DATE_OF_BIRTH);
        int clientCitizenshipNumberPos = cursor.getColumnIndex(DatabaseContract.FamilyMemberEntry.COLUMN_CITIZENSHIP_NUMBER);
        int clientRelationToHead = cursor.getColumnIndex(DatabaseContract.FamilyMemberEntry.COLUMN_MEMBER_RELATION_TO_HEAD);
        int clientImageUrlPos = cursor.getColumnIndex(DatabaseContract.FamilyMemberEntry.COLUMN_MEMBER_IMAGE_URL);
        int clientHeadIdPos = cursor.getColumnIndex(DatabaseContract.FamilyMemberEntry.COLUMN_HEAD_ID);

        cursor.moveToNext();

        ClientFamilyMemberModel client = new ClientFamilyMemberModel();
        client.setFirstName(cursor.getString(clientFirstNamePos));
        client.setMiddleName(cursor.getString(clientMiddleNamePos));
        client.setLastName(cursor.getString(clientLastNamePos));
        client.setDateOfBirth(cursor.getString(clientDateOfBirthPos));
        client.setCitizenshipNumber(cursor.getString(clientCitizenshipNumberPos));
        client.setId(cursor.getInt(clientIdPos));
        client.setRelationToHead(cursor.getString(clientRelationToHead));
        client.setImageUrl(cursor.getString(clientImageUrlPos));
        client.setHeadId(cursor.getInt(clientHeadIdPos));

        userInfoOpenHelper.close();

        return client;
    }

    public static int deleteFamilyMember(int id){
        SQLiteDatabase db = userInfoOpenHelper.getWritableDatabase();
        String selection = DatabaseContract.FamilyMemberEntry._ID + " =? ";
        String selectionArgs[] = {String.valueOf(id)};

        int delete = db.delete(DatabaseContract.FamilyMemberEntry.TABLE_NAME,selection,selectionArgs);
        userInfoOpenHelper.close();

        return delete;
    }
}
