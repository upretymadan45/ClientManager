package com.helper.little.uprety45.userinfo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.helper.little.uprety45.userinfo.R;
import com.helper.little.uprety45.userinfo.database.UserInfoOpenHelper;
import com.helper.little.uprety45.userinfo.fragments.AddFamilyHeadFragment;

public class MainActivity extends AppCompatActivity {
    private UserInfoOpenHelper userInfoOpenHelper;
    private boolean showAddClientFragment = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userInfoOpenHelper = new UserInfoOpenHelper(this);
        userInfoOpenHelper.getWritableDatabase();
    }

    @Override
    protected void onDestroy() {
        userInfoOpenHelper.close();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.loadClients:
                startActivity(new Intent(MainActivity.this,ClientListActivity.class));
                break;
        }
        return true;
    }

    public void addClient(View view) {
        showAddClientFragment = !showAddClientFragment;
        Button button = (Button) view;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if(showAddClientFragment){
            fragmentTransaction.replace(R.id.group, AddFamilyHeadFragment.getFragmentInstance(), AddFamilyHeadFragment.TAG);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            button.setText("Cancel add client");
        } else {
            AddFamilyHeadFragment fragment = (AddFamilyHeadFragment) getSupportFragmentManager().findFragmentByTag(AddFamilyHeadFragment.TAG);

            if(fragment!=null) {
                fragmentTransaction.remove(fragment);
                fragmentTransaction.commit();
                button.setText("Add client");
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
