package com.helper.little.uprety45.userinfo.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.helper.little.uprety45.userinfo.R;
import com.helper.little.uprety45.userinfo.activities.ClientDetailsActivity;
import com.helper.little.uprety45.userinfo.models.ClientModel;

import java.util.List;

public class ClientListAdapter extends RecyclerView.Adapter<ClientListAdapter.ViewHolder> {
    public static final String TAG = ClientListAdapter.class.getSimpleName();
    private List<ClientModel> clients;
    private Context context;

    public ClientListAdapter(List<ClientModel> clients, Context context) {
        this.clients = clients;
        this.context = context;
    }

    public Context getContext(){
        return this.context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View onlineUserView = inflater.inflate(R.layout.client_list_row,parent,false);

        ViewHolder viewHolder = new ViewHolder(onlineUserView);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ClientModel client = clients.get(position);

        TextView clientName = holder.clientName;
        TextView clientCitizenshipNumber = holder.clientCitizenshipNumber;

        clientName.setText(client.getFirstName() + " "+ client.getLastName());

        clientCitizenshipNumber.setText("Citizenship No: "+client.getCitizenshipNumber());

        Log.d(TAG, "onBindViewHolder: "+client.getImageUrl());

        Glide.with(getContext())
                .load("file://"+client.getImageUrl())
                //.error(R.drawable.ic_account_box_black)
                .into(holder.clientImage);
    }

    @Override
    public int getItemCount() {
        return clients.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView clientName, clientCitizenshipNumber;
        private ImageView clientImage;

        public ViewHolder(View itemView) {
            super(itemView);

            clientName = itemView.findViewById(R.id.clientName);
            clientCitizenshipNumber = itemView.findViewById(R.id.clientCitizenshipNumber);
            clientImage = (ImageView) itemView.findViewById(R.id.clientImage);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if(position!=RecyclerView.NO_POSITION){
                ClientModel client = clients.get(position);
                Intent intent = new Intent(getContext(), ClientDetailsActivity.class);
                intent.putExtra("headId",client.getId());
                getContext().startActivity(intent);
            }
        }
    }
}
