package com.helper.little.uprety45.userinfo.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.helper.little.uprety45.userinfo.models.ClientModel;

import java.util.ArrayList;
import java.util.List;

import static com.helper.little.uprety45.userinfo.database.DatabaseContract.*;

public final class ClientDAO {
    private static List<ClientModel> clientModels;
    private final Context context;
    private static UserInfoOpenHelper userInfoOpenHelper;

    public ClientDAO(Context context) {
        this.context = context;
        clientModels = new ArrayList<ClientModel>();
        userInfoOpenHelper = new UserInfoOpenHelper(context);
    }

    public static List<ClientModel> loadClients(){
        SQLiteDatabase db = userInfoOpenHelper.getReadableDatabase();

        String [] projection = {
                FamilyHeadEntry._ID,
                FamilyHeadEntry.COLUMN_HEAD_FIRST_NAME,
                FamilyHeadEntry.COLUMN_HEAD_MIDDLE_NAME,
                FamilyHeadEntry.COLUMN_HEAD_LAST_NAME,
                FamilyHeadEntry.COLUMN_DATE_OF_BIRTH,
                FamilyHeadEntry.COLUMN_CITIZENSHIP_NUMBER,
                FamilyHeadEntry.COLUMN_HEAD_IMAGE_URL,
                };

        Cursor cursor = db.query(FamilyHeadEntry.TABLE_NAME,projection,null,null,null,null, FamilyHeadEntry.COLUMN_HEAD_FIRST_NAME);

        int clientIdPos = cursor.getColumnIndex(FamilyHeadEntry._ID);
        int clientFirstNamePos = cursor.getColumnIndex(FamilyHeadEntry.COLUMN_HEAD_FIRST_NAME);
        int clientLastNamePos = cursor.getColumnIndex(FamilyHeadEntry.COLUMN_HEAD_LAST_NAME);
        int clientCitizenshipNumberPos = cursor.getColumnIndex(FamilyHeadEntry.COLUMN_CITIZENSHIP_NUMBER);
        int clientImageUrlPos = cursor.getColumnIndex(FamilyHeadEntry.COLUMN_HEAD_IMAGE_URL);

        while(cursor.moveToNext()){

            ClientModel client = new ClientModel();
            client.setFirstName(cursor.getString(clientFirstNamePos));
            client.setLastName(cursor.getString(clientLastNamePos));
            client.setId(cursor.getInt(clientIdPos));
            client.setCitizenshipNumber(cursor.getString(clientCitizenshipNumberPos));
            client.setImageUrl(cursor.getString(clientImageUrlPos));

            clientModels.add(client);
        }

        userInfoOpenHelper.close();

        return clientModels;
    }

    public static ClientModel getClient(String id){

        SQLiteDatabase db = userInfoOpenHelper.getReadableDatabase();
        String [] projection = {
                FamilyHeadEntry.COLUMN_HEAD_FIRST_NAME,
                FamilyHeadEntry.COLUMN_HEAD_MIDDLE_NAME,
                FamilyHeadEntry.COLUMN_HEAD_LAST_NAME,
                FamilyHeadEntry.COLUMN_CITIZENSHIP_NUMBER,
                FamilyHeadEntry.COLUMN_HEAD_IMAGE_URL,
                FamilyHeadEntry.COLUMN_DATE_OF_BIRTH,
                FamilyHeadEntry._ID};

        String  selection = FamilyHeadEntry._ID +" = ?";
        String selectionArgs[] = {id};

        Cursor cursor = db.query(FamilyHeadEntry.TABLE_NAME,projection,selection,selectionArgs,null,null, FamilyHeadEntry.COLUMN_HEAD_FIRST_NAME);

        int clientIdPos = cursor.getColumnIndex(FamilyHeadEntry._ID);
        int clientFirstNamePos = cursor.getColumnIndex(FamilyHeadEntry.COLUMN_HEAD_FIRST_NAME);
        int clientMiddleNamePos = cursor.getColumnIndex(FamilyHeadEntry.COLUMN_HEAD_MIDDLE_NAME);
        int clientLastNamePos = cursor.getColumnIndex(FamilyHeadEntry.COLUMN_HEAD_LAST_NAME);
        int clientCitizenshipNumberPos = cursor.getColumnIndex(FamilyHeadEntry.COLUMN_CITIZENSHIP_NUMBER);
        int dateOfBirthPos = cursor.getColumnIndex(FamilyHeadEntry.COLUMN_DATE_OF_BIRTH);
        int imageUrlPos = cursor.getColumnIndex(FamilyHeadEntry.COLUMN_HEAD_IMAGE_URL);

        cursor.moveToNext();

        ClientModel client = new ClientModel();
        client.setId(cursor.getInt(clientIdPos));
        client.setFirstName(cursor.getString(clientFirstNamePos));
        client.setMiddleName(cursor.getString(clientMiddleNamePos));
        client.setLastName(cursor.getString(clientLastNamePos));
        client.setCitizenshipNumber(cursor.getString(clientCitizenshipNumberPos));
        client.setDateOfBirth(cursor.getString(dateOfBirthPos));
        client.setImageUrl(cursor.getString(imageUrlPos));

        userInfoOpenHelper.close();

        return client;
    }

    public static int deleteClient(int id){
        SQLiteDatabase db = userInfoOpenHelper.getWritableDatabase();
        String selection = FamilyHeadEntry._ID + " =? ";
        String selectionArgs[] = {String.valueOf(id)};

        int delete = db.delete(FamilyHeadEntry.TABLE_NAME,selection,selectionArgs);
        userInfoOpenHelper.close();

        return delete;
    }
}
