package com.helper.little.uprety45.userinfo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.helper.little.uprety45.userinfo.R;
import com.helper.little.uprety45.userinfo.adapters.ClientFamilyMemberListAdapter;
import com.helper.little.uprety45.userinfo.database.ClientDAO;
import com.helper.little.uprety45.userinfo.database.ClientFamilyMemberDAO;
import com.helper.little.uprety45.userinfo.database.UserInfoOpenHelper;
import com.helper.little.uprety45.userinfo.dialogs.YesNoConfirmationDialog;
import com.helper.little.uprety45.userinfo.dialogs.YesNoConfirmationDialog.IOnDialogButtonClickListener;
import com.helper.little.uprety45.userinfo.fragments.AddFamilyHeadFragment;
import com.helper.little.uprety45.userinfo.fragments.AddFamilyMemberFragment;
import com.helper.little.uprety45.userinfo.models.ClientFamilyMemberModel;
import com.helper.little.uprety45.userinfo.models.ClientModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ClientDetailsActivity extends AppCompatActivity implements IOnDialogButtonClickListener {
    public static final String TAG = ClientDetailsActivity.class.getSimpleName();
    private UserInfoOpenHelper userInfoOpenHelper;
    private ClientModel clientModel = new ClientModel();
    private TextView firstName, middleName,lastName,dateOfBirth,citizenshipNumber,middleNameLabel;
    private RecyclerView rvFamilyMembers;
    private int headId;
    private LinearLayout clientDetailsLayout;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_details);

        imageView = (ImageView) findViewById(R.id.imageView);

        firstName = (TextView) findViewById(R.id.first_name);
        middleName = (TextView) findViewById(R.id.middle_name);
        middleNameLabel = (TextView) findViewById(R.id.middle_name_label);
        lastName = (TextView) findViewById(R.id.last_name);
        dateOfBirth = (TextView) findViewById(R.id.date_of_birth);
        citizenshipNumber = (TextView) findViewById(R.id.citizenship_number);

        rvFamilyMembers = (RecyclerView) findViewById(R.id.rv_family_members);

        clientDetailsLayout = (LinearLayout) findViewById(R.id.clientDetails);

        userInfoOpenHelper = new UserInfoOpenHelper(this);

        Intent intent = getIntent();
        headId = intent.getIntExtra("headId",0);

        loadClients(String.valueOf(headId));
        loadClientFamilyMembers(String.valueOf(headId));
    }

    @Override
    protected void onDestroy() {
        userInfoOpenHelper.close();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_client_details_activity,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id){
            case R.id.add_new_member:
                AddFamilyMemberFragment fragment = new AddFamilyMemberFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("headId",headId);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.group,fragment,AddFamilyMemberFragment.TAG);
                fragmentTransaction.commit();
                clientDetailsLayout.setVisibility(View.INVISIBLE);
                break;

            case R.id.edit_member:
                Bundle bundle1 = new Bundle();
                bundle1.putParcelable("ClientModel",clientModel);
                AddFamilyHeadFragment familyHeadFragment = new AddFamilyHeadFragment();
                familyHeadFragment.setArguments(bundle1);
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction1 = fragmentManager1.beginTransaction();
                fragmentTransaction1.replace(R.id.group,familyHeadFragment,AddFamilyHeadFragment.TAG);
                fragmentTransaction1.commit();
                clientDetailsLayout.setVisibility(View.INVISIBLE);
                break;

            case R.id.delete_member:
                YesNoConfirmationDialog yesNoConfirmationDialog = new YesNoConfirmationDialog();
                yesNoConfirmationDialog.setCancelable(false);
                yesNoConfirmationDialog.show(getSupportFragmentManager(),"");
                break;
        }

        return true;
    }

    public void loadClients(String id){
        new ClientDAO(this);
        ClientModel client = ClientDAO.getClient(id);

        clientModel = client;

        firstName.setText(client.getFirstName());
        if(!client.getMiddleName().equals("")){
            middleName.setVisibility(View.VISIBLE);
            middleNameLabel.setVisibility(View.VISIBLE);
            middleName.setText(client.getMiddleName());
        } else {
            middleName.setVisibility(View.GONE);
            middleNameLabel.setVisibility(View.GONE);
        }
        lastName.setText(client.getLastName());
        dateOfBirth.setText(client.getDateOfBirth());
        citizenshipNumber.setText(client.getCitizenshipNumber());
        Log.d(TAG, "loadClients: "+client.getImageUrl());

        Picasso.with(this)
                .load("file://"+client.getImageUrl())
                .placeholder(R.drawable.ic_account_box_black)
                .into(imageView);

    }

    public void loadClientFamilyMembers(String headId){

        List<ClientFamilyMemberModel> familyMembers = new ArrayList<ClientFamilyMemberModel>();

        new ClientFamilyMemberDAO(this);
        familyMembers = ClientFamilyMemberDAO.loadClientFamilyMembers(headId);

        ClientFamilyMemberListAdapter adapter = new ClientFamilyMemberListAdapter(familyMembers,this);
        rvFamilyMembers.setAdapter(adapter);
        rvFamilyMembers.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public void onPositiveButtonClick() {
        new ClientDAO(this);
        int delete = ClientDAO.deleteClient(headId);

        if(delete>0){
            makeToast("Client deleted",Toast.LENGTH_LONG);
            startActivity(new Intent(ClientDetailsActivity.this,ClientListActivity.class));
        } else {
            makeToast("Failed to delete client. Please delete its member first",Toast.LENGTH_LONG);
        }
    }

    private void makeToast(String message,int lenght){
        Toast.makeText(this,message,lenght).show();
    }
}
