package com.helper.little.uprety45.userinfo.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;

import com.helper.little.uprety45.userinfo.activities.ClientDetailsActivity;
import com.helper.little.uprety45.userinfo.fragments.AddFamilyMemberFragment;

public class ConfirmationDialog extends DialogFragment {

    private AddFamilyMemberFragment addFamilyMemberFragment;
    private int headId;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        headId = bundle.getInt("headId",0);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Member Added. Do you want to add another family member?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                addFamilyMemberFragment.clearView();
            }
        })
        .setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(getActivity(), ClientDetailsActivity.class);
                intent.putExtra("headId",headId);
                startActivity(intent);
            }
        });

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        FragmentManager fragmentManager = getFragmentManager();
        addFamilyMemberFragment= (AddFamilyMemberFragment) fragmentManager.findFragmentByTag(AddFamilyMemberFragment.TAG);
    }
}
