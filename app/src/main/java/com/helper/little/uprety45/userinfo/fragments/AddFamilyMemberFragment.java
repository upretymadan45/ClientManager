package com.helper.little.uprety45.userinfo.fragments;


import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.helper.little.uprety45.userinfo.R;
import com.helper.little.uprety45.userinfo.activities.ClientDetailsActivity;
import com.helper.little.uprety45.userinfo.database.UserInfoOpenHelper;
import com.helper.little.uprety45.userinfo.dialogs.ConfirmationDialog;
import com.helper.little.uprety45.userinfo.helpers.ImageFileCreator;
import com.helper.little.uprety45.userinfo.models.ClientFamilyMemberModel;

import java.io.File;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;
import static com.helper.little.uprety45.userinfo.database.DatabaseContract.FamilyMemberEntry;

public class AddFamilyMemberFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = AddFamilyMemberFragment.class.getSimpleName();
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private String capturedImageUrl;
    private Button btnSave, btnTakePicture;
    private EditText firstName, middleName,lastName,citizenshipNumber,dateOfBirth,relationToHead;
    private UserInfoOpenHelper userInfoOpenHelper;
    private int headId;
    private int id;
    private boolean isEditMode = false;
    public AddFamilyMemberFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_family_member, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        userInfoOpenHelper = new UserInfoOpenHelper(getActivity());

        btnSave = (Button) view.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

        btnTakePicture = (Button) view.findViewById(R.id.btnTakePicture);
        btnTakePicture.setOnClickListener(this);

        firstName = (EditText) view.findViewById(R.id.first_name);
        middleName = (EditText) view.findViewById(R.id.middle_name);
        lastName = (EditText) view.findViewById(R.id.last_name);
        citizenshipNumber = (EditText) view.findViewById(R.id.citizenship_number);
        dateOfBirth = (EditText) view.findViewById(R.id.date_of_birth);
        relationToHead = (EditText) view.findViewById(R.id.relation_to_head);

        Bundle bundle = getArguments();
        headId = bundle.getInt("headId");

        Bundle bundleArg = getArguments();
        if(bundleArg==null) return;
        ClientFamilyMemberModel clientFamilyMemberModel = bundleArg.getParcelable("ClientFamilyMemberModel");
        if(clientFamilyMemberModel!=null){
            firstName.setText(clientFamilyMemberModel.getFirstName());
            middleName.setText(clientFamilyMemberModel.getMiddleName());
            lastName.setText(clientFamilyMemberModel.getLastName());
            citizenshipNumber.setText(clientFamilyMemberModel.getCitizenshipNumber());
            dateOfBirth.setText(clientFamilyMemberModel.getDateOfBirth());
            relationToHead.setText(clientFamilyMemberModel.getRelationToHead());
            id = clientFamilyMemberModel.getId();
            isEditMode = true;
        }
    }

    private int createFamilyMember(int headId,boolean editMode) {
        ContentValues values = new ContentValues();
        values.put(FamilyMemberEntry.COLUMN_MEMBER_FIRST_NAME,firstName.getText().toString());
        values.put(FamilyMemberEntry.COLUMN_MEMBER_MIDDLE_NAME,middleName.getText().toString());
        values.put(FamilyMemberEntry.COLUMN_MEMBER_LAST_NAME,lastName.getText().toString());
        values.put(FamilyMemberEntry.COLUMN_CITIZENSHIP_NUMBER,citizenshipNumber.getText().toString());
        values.put(FamilyMemberEntry.COLUMN_DATE_OF_BIRTH,dateOfBirth.getText().toString());
        values.put(FamilyMemberEntry.COLUMN_MEMBER_RELATION_TO_HEAD,relationToHead.getText().toString());
        values.put(FamilyMemberEntry.COLUMN_HEAD_ID,headId);
        if(capturedImageUrl!=null)
        values.put(FamilyMemberEntry.COLUMN_MEMBER_IMAGE_URL,capturedImageUrl);

        if(!editMode){
            SQLiteDatabase db = userInfoOpenHelper.getWritableDatabase();
            return (int)db.insert(FamilyMemberEntry.TABLE_NAME,null,values);
        } else {
            SQLiteDatabase db = userInfoOpenHelper.getWritableDatabase();
            String selection = FamilyMemberEntry._ID + " =? ";
            String selectionArgs[] = {String.valueOf(id)};
            return (int)db.update(FamilyMemberEntry.TABLE_NAME,values,selection,selectionArgs);
        }
    }

    @Override
    public void onClick(View v) {
        if(v==btnSave){
            if(firstName.getText().toString().equals("")){
                firstName.setError("Cannot be blank");
                return;
            }

            if(lastName.getText().toString().equals("")){
                lastName.setError("Cannot be blank");
                return;
            }

            if(citizenshipNumber.getText().toString().equals("")){
                citizenshipNumber.setError("Cannot be blank");
                return;
            }

            if(dateOfBirth.getText().toString().equals("")){
                dateOfBirth.setError("Cannot be blank");
                return;
            }

            if(relationToHead.getText().toString().equals("")){
                relationToHead.setError("Cannot be blank");
                return;
            }

            if(!isEditMode){
                int created = createFamilyMember(headId,false);

                if(created>0){
                    ConfirmationDialog dialog = new ConfirmationDialog();
                    Bundle bundle = new Bundle();
                    bundle.putInt("headId",headId);
                    dialog.setArguments(bundle);
                    dialog.show(getFragmentManager(),"");
                    dialog.setCancelable(false);
                }
            } else{
                createFamilyMember(headId,true);
                isEditMode = false;
                Toast.makeText(getActivity(),"Member updated.",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity(), ClientDetailsActivity.class);
                intent.putExtra("headId",headId);
                startActivity(intent);
            }

        }

        if(v==btnTakePicture){
            Log.d(TAG, "onClick: take picture");
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                File photoFile = null;
                try {
                    photoFile = ImageFileCreator.createImageFile(getActivity());
                    capturedImageUrl = photoFile.getAbsolutePath();
                } catch (IOException ex) {

                }
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(getActivity(),
                            "com.helper.little.uprety45.fileprovider",
                            photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        }
        
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode==RESULT_OK){
            Toast.makeText(getActivity(),"Photo successfully caputred",Toast.LENGTH_LONG).show();
        }
    }

    public static AddFamilyMemberFragment getFragmentInstance(){
        return new AddFamilyMemberFragment();
    }

    @Override
    public void onDetach() {
        if(userInfoOpenHelper!=null)
            userInfoOpenHelper.close();
        super.onDetach();
    }

    public void clearView() {
        firstName.setText("");
        middleName.setText("");
        lastName.setText("");
        citizenshipNumber.setText("");
        dateOfBirth.setText("");
        relationToHead.setText("");
    }

}
