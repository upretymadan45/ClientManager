package com.helper.little.uprety45.userinfo.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.helper.little.uprety45.userinfo.R;
import com.helper.little.uprety45.userinfo.dialogs.FamilyMemberDetailsDialog;
import com.helper.little.uprety45.userinfo.models.ClientFamilyMemberModel;

import java.util.List;

public class ClientFamilyMemberListAdapter extends RecyclerView.Adapter<ClientFamilyMemberListAdapter.ViewHolder> {
    public static final String TAG = ClientFamilyMemberListAdapter.class.getSimpleName();
    private List<ClientFamilyMemberModel> familyMembers;
    private Context context;

    public ClientFamilyMemberListAdapter(List<ClientFamilyMemberModel> familyMembers, Context context) {
        this.familyMembers = familyMembers;
        this.context = context;
    }

    private Context getContext(){
        return this.context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View familyMemberRow = inflater.inflate(R.layout.family_members_row,parent,false);

        ViewHolder viewHolder = new ViewHolder(familyMemberRow);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ClientFamilyMemberModel familyMemberModel = familyMembers.get(position);
        holder.memberFirstName.setText(familyMemberModel.getFirstName());
        holder.memberRelationToHead.setText("Relation: "+familyMemberModel.getRelationToHead());
        Log.d(TAG, "onBindViewHolder: "+familyMemberModel.getImageUrl());
        Glide.with(getContext())
                .load("file://"+familyMemberModel.getImageUrl())
                //.error(R.drawable.ic_account_box_black)
                .into(holder.clientImage);
    }

    @Override
    public int getItemCount() {
        return familyMembers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView memberFirstName,memberRelationToHead;
        private ImageView clientImage;

        public ViewHolder(View itemView) {
            super(itemView);

            memberFirstName = itemView.findViewById(R.id.member_first_name);
            memberRelationToHead = itemView.findViewById(R.id.member_relation_to_head);
            clientImage = (ImageView) itemView.findViewById(R.id.clientImage);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if(position!=RecyclerView.NO_POSITION){
                ClientFamilyMemberModel clientFamilyMemberModel = familyMembers.get(position);

                Bundle bundle = new Bundle();
                bundle.putInt("id",clientFamilyMemberModel.getId());

                FamilyMemberDetailsDialog familyMemberDetailsDialog = new FamilyMemberDetailsDialog();
                familyMemberDetailsDialog.setArguments(bundle);

                FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();

                familyMemberDetailsDialog.show(fragmentManager,FamilyMemberDetailsDialog.TAG);
            }
        }
    }
}
