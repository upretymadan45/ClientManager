package com.helper.little.uprety45.userinfo.models;

import android.os.Parcel;
import android.os.Parcelable;

public class ClientModel implements Parcelable {
    private int id;
    private String firstName;
    private String middleName;
    private String lastName;
    private String citizenshipNumber;
    private String dateOfBirth;
    private String imageUrl;

    public ClientModel() {
    }

    protected ClientModel(Parcel in) {
        id = in.readInt();
        firstName = in.readString();
        middleName = in.readString();
        lastName = in.readString();
        citizenshipNumber = in.readString();
        dateOfBirth = in.readString();
        imageUrl = in.readString();
    }

    public static final Creator<ClientModel> CREATOR = new Creator<ClientModel>() {
        @Override
        public ClientModel createFromParcel(Parcel in) {
            return new ClientModel(in);
        }

        @Override
        public ClientModel[] newArray(int size) {
            return new ClientModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCitizenshipNumber() {
        return citizenshipNumber;
    }

    public void setCitizenshipNumber(String citizenshipNumber) {
        this.citizenshipNumber = citizenshipNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(firstName);
        dest.writeString(middleName);
        dest.writeString(lastName);
        dest.writeString(citizenshipNumber);
        dest.writeString(dateOfBirth);
        dest.writeString(imageUrl);
    }
}
