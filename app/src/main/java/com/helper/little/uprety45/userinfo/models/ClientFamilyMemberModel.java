package com.helper.little.uprety45.userinfo.models;

import android.os.Parcel;
import android.os.Parcelable;

public class ClientFamilyMemberModel implements Parcelable{
    private int id;
    private String firstName;
    private String middleName;
    private String lastName;
    private String citizenshipNumber;
    private String dateOfBirth;
    private String imageUrl;
    private int headId;
    private String relationToHead;

    public ClientFamilyMemberModel() {
    }

    protected ClientFamilyMemberModel(Parcel in) {
        id = in.readInt();
        firstName = in.readString();
        middleName = in.readString();
        lastName = in.readString();
        citizenshipNumber = in.readString();
        dateOfBirth = in.readString();
        imageUrl = in.readString();
        headId = in.readInt();
        relationToHead = in.readString();
    }

    public static final Creator<ClientFamilyMemberModel> CREATOR = new Creator<ClientFamilyMemberModel>() {
        @Override
        public ClientFamilyMemberModel createFromParcel(Parcel in) {
            return new ClientFamilyMemberModel(in);
        }

        @Override
        public ClientFamilyMemberModel[] newArray(int size) {
            return new ClientFamilyMemberModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCitizenshipNumber() {
        return citizenshipNumber;
    }

    public void setCitizenshipNumber(String citizenshipNumber) {
        this.citizenshipNumber = citizenshipNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getHeadId() {
        return headId;
    }

    public void setHeadId(int headId) {
        this.headId = headId;
    }

    public String getRelationToHead() {
        return relationToHead;
    }

    public void setRelationToHead(String relationToHead) {
        this.relationToHead = relationToHead;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(firstName);
        dest.writeString(middleName);
        dest.writeString(lastName);
        dest.writeString(citizenshipNumber);
        dest.writeString(dateOfBirth);
        dest.writeString(imageUrl);
        dest.writeInt(headId);
        dest.writeString(relationToHead);
    }
}
