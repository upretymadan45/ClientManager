package com.helper.little.uprety45.userinfo.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;

import com.helper.little.uprety45.userinfo.R;
import com.helper.little.uprety45.userinfo.adapters.ClientListAdapter;
import com.helper.little.uprety45.userinfo.database.ClientDAO;
import com.helper.little.uprety45.userinfo.database.UserInfoOpenHelper;
import com.helper.little.uprety45.userinfo.models.ClientModel;

import java.util.ArrayList;
import java.util.List;

public class ClientListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    private static final String TAG =  ClientListActivity.class.getSimpleName() ;
    private RecyclerView rvClientList;
    private UserInfoOpenHelper userInfoOpenHelper;
    private List<ClientModel> clients;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_list);
        clients = new ArrayList<ClientModel>();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.app_name);

        userInfoOpenHelper = new UserInfoOpenHelper(this);

        userInfoOpenHelper.getReadableDatabase();

        rvClientList = (RecyclerView) findViewById(R.id.clientList);

        loadClients();

    }

    @Override
    protected void onDestroy() {
        userInfoOpenHelper.close();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_client_list_activity,menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.txtSearch).getActionView();
        searchView.setQueryHint("Enter search text");
        searchView.setIconified(false);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(this);

        return true;
    }

    public void loadClients(){
        clients.clear();

        ClientDAO clientDAO = new ClientDAO(this);
        clients = ClientDAO.loadClients();

        ClientListAdapter adapter = new ClientListAdapter(clients,this);
        rvClientList.setAdapter(adapter);
        rvClientList.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.d(TAG, "onQueryTextSubmit: "+query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(final String newText) {
        Log.d(TAG, "onQueryTextChange: "+newText);

        List<ClientModel> searchList = new ArrayList<ClientModel>();

        for (ClientModel client:clients) {
            if(client.getFirstName().contains(newText) ||
               client.getLastName().contains(newText)  ||
               client.getCitizenshipNumber().contains(newText))
            {
                searchList.add(client);
            }
        }

        setSearch(searchList);

        return true;
    }

    public void setSearch(List<ClientModel> clients){
        ClientListAdapter adapter = new ClientListAdapter(clients,this);
        rvClientList.setAdapter(adapter);
        rvClientList.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
