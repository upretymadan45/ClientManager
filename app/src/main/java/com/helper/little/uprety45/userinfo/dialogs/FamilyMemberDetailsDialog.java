package com.helper.little.uprety45.userinfo.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.helper.little.uprety45.userinfo.R;
import com.helper.little.uprety45.userinfo.activities.ClientDetailsActivity;
import com.helper.little.uprety45.userinfo.database.ClientFamilyMemberDAO;
import com.helper.little.uprety45.userinfo.database.UserInfoOpenHelper;
import com.helper.little.uprety45.userinfo.fragments.AddFamilyMemberFragment;
import com.helper.little.uprety45.userinfo.models.ClientFamilyMemberModel;

import static com.helper.little.uprety45.userinfo.dialogs.YesNoConfirmationDialog.IOnDialogButtonClickListener;


public class FamilyMemberDetailsDialog extends DialogFragment implements View.OnClickListener,IOnDialogButtonClickListener {
    public static final String TAG = FamilyMemberDetailsDialog.class.getSimpleName();
    private int id;
    private TextView firstName, middleName,lastName,dateOfBirth,citizenshipNumber,relationToHead,middleNameLabel;
    private ImageView imageView;
    private UserInfoOpenHelper userInfoOpenHelper;
    private Button btnEdit,btnDelete;
    private ClientFamilyMemberModel clientFamilyMemberModel = new ClientFamilyMemberModel();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        userInfoOpenHelper = new UserInfoOpenHelper(getActivity());

        Bundle bundle = getArguments();
        id = bundle.getInt("id");
        Log.d(TAG, "onCreateDialog: "+id);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.family_member_details,null);
        builder = builder.setView(view);

        builder.setTitle("Member Details");

        firstName = (TextView) view.findViewById(R.id.first_name);
        middleName = (TextView) view.findViewById(R.id.middle_name);
        middleNameLabel = (TextView) view.findViewById(R.id.middle_name_label);
        lastName = (TextView) view.findViewById(R.id.last_name);
        dateOfBirth = (TextView) view.findViewById(R.id.date_of_birth);
        citizenshipNumber = (TextView) view.findViewById(R.id.citizenship_number);
        relationToHead = (TextView) view.findViewById(R.id.relation_to_head);

        btnEdit = (Button) view.findViewById(R.id.btnEdit);
        btnDelete = (Button)view.findViewById(R.id.btnDelete);
        btnEdit.setOnClickListener(this);
        btnDelete.setOnClickListener(this);


        imageView = (ImageView) view.findViewById(R.id.imageView);
        loadClientFamilyMembers(String.valueOf(id));

        return builder.create();
    }

    @Override
    public void onDetach() {
        userInfoOpenHelper.close();
        super.onDetach();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    public void loadClientFamilyMembers(String id){

        new ClientFamilyMemberDAO(getActivity());
        ClientFamilyMemberModel client = ClientFamilyMemberDAO.getClientFamilyMember(id);

        clientFamilyMemberModel = client;

        Log.d(TAG, "loadClientFamilyMembers: "+client.getImageUrl());

        firstName.setText(client.getFirstName());
        if(!client.getMiddleName().equals("")){
            middleName.setVisibility(View.VISIBLE);
            middleNameLabel.setVisibility(View.VISIBLE);
            middleName.setText(client.getMiddleName());
        } else {
            middleName.setVisibility(View.GONE);
            middleNameLabel.setVisibility(View.GONE);
        }
        lastName.setText(client.getLastName());
        dateOfBirth.setText(client.getDateOfBirth());
        citizenshipNumber.setText(client.getCitizenshipNumber());
        relationToHead.setText(client.getRelationToHead());

        Glide.with(getActivity())
                .load("file://"+client.getImageUrl())
                //.placeholder(R.drawable.ic_account_box_black)
                .into(imageView);
    }

    @Override
    public void onClick(View v) {
        if(v==btnEdit){
            AddFamilyMemberFragment addFamilyMemberFragment = new AddFamilyMemberFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable("ClientFamilyMemberModel",clientFamilyMemberModel);
            bundle.putInt("headId",clientFamilyMemberModel.getHeadId());
            addFamilyMemberFragment.setArguments(bundle);
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.group,addFamilyMemberFragment,AddFamilyMemberFragment.TAG);
            fragmentTransaction.commit();

            getDialog().dismiss();
        }

        if(v==btnDelete){
            YesNoConfirmationDialog yesNoConfirmationDialog = new YesNoConfirmationDialog();
            yesNoConfirmationDialog.setCancelable(false);
            FragmentManager fragmentManager = getFragmentManager();
            yesNoConfirmationDialog.show(fragmentManager,"");

        }
    }

    @Override
    public void onPositiveButtonClick() {
        new ClientFamilyMemberDAO(getActivity());
        int delete = ClientFamilyMemberDAO.deleteFamilyMember(id);

        if(delete>0) {
            Toast.makeText(getActivity(), "Member deleted", Toast.LENGTH_LONG).show();

            Intent intent = new Intent(getActivity(), ClientDetailsActivity.class);
            intent.putExtra("headId", clientFamilyMemberModel.getHeadId());
            startActivity(intent);
        } else {
            Toast.makeText(getActivity(), "Failed to delete member", Toast.LENGTH_LONG).show();
        }
    }
}
